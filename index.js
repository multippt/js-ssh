var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Client = require('ssh2').Client;

// Pages
app.use(express.static(__dirname + '/'));

http.listen(4000, function() {
	console.log('listening on *:4000');
});

function ab2str(buf) {
	return String.fromCharCode.apply(null, new Uint16Array(buf));
}

// Handle client connections
io.on('connection', function(socket) {
	var ssh = new Client();
	var connected = false;
	var activeStream = null;
	socket.on('disconnect', function() {
		if (connected) {
			ssh.end();
		}
	});
	socket.on('login', function(data) {
		// Perform login
		data = JSON.parse(data);
		data.port = 22;
		try {
			ssh.on('ready', function() {
				connected = true;

				// Setup shell
				ssh.shell(function(err, stream) {
					if (err) {
						activeStream = null;
						socket.emit('logout');
					}
					activeStream = stream;
					stream.on("close", function() {
						socket.emit('logout');
					});
					stream.on("data", function(data) {
						socket.emit('stdout', ab2str(data));
					});
					stream.stderr.on("data", function(data) {
						socket.emit('stdout', ab2str(data));
					});
				});

				socket.emit('login', true);
			}).connect(data);
		} catch (ex) {
			socket.emit('login', false);
		}
	});
	socket.on('exec', function(data) {
		if (activeStream) {
			activeStream.write(data);
		}
	});
});